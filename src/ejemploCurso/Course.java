/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploCurso;

/**
 *
 * @author admin
 */
public class Course {
    
    private int id;
    
    private String name;
    
    private int maxCapacity = 5;
    
    private CourseStudent[] courseStudents;
    
    public Course(int _id, String _name, int _capacity){
        id = _id;
        name = _name;
        maxCapacity = _capacity;                
        courseStudents = new CourseStudent[maxCapacity];
    }
    
    public void register(Student student){        
        int emptyCell = -1;
        for(int i = 0; i < maxCapacity && emptyCell == -1; i++){
            if( courseStudents[i] == null )
                emptyCell = i;                        
        }
        courseStudents[emptyCell] = new CourseStudent(student, 100D);
    }
    
    public int amountOfStudents(){
        int count = 0;
        for(int i = 0; i < maxCapacity ; i++){
            if( courseStudents[i] != null )
                count++;
        }
        return count;
    }
    
    private void reduceMark(double mark) {        
        for(int i = 0; i < maxCapacity ; i++){
            if( courseStudents[i] != null )
                courseStudents[i].reduceMark(mark);
        }
    }
    
    private void printDetails() {
        String output = "Course: " + name + " id: " + id + "\n";
        output = output + "-----------------------\n";
        for(int i = 0; i < maxCapacity ; i++){
            if( courseStudents[i] != null )
                output = output + courseStudents[i].toString() + "\n";
        }
        System.out.println(output);
    }
    
    public static void main(String[] args) {
        Course course = new Course(1, "java_inicial", 4);        
        course.register(new Student(10, "Ariel", "Chimba", 21));
        course.register(new Student(20, "Oliver", "Pinto", 25));
        System.out.println("registered students: " + course.amountOfStudents());        
        course.printDetails();
        course.reduceMark(35.1D);
        course.printDetails();        
    }

    

    
    
}
